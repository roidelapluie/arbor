# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Based in part upon 'vala.exlib' which is:
#   Copyright 2016 Quentin "Sardem FF7" Glidic <sardemff7+exherbo@sardemff7.net>

# This exlib is for packages using the wxWidgets toolkit.
# It mainly handles exporting the WX_CONFIG_NAME environment variable
# which determines the `wx-config` binary to be used in the
# WX_CONFIG_CHECK macro from wxGTK's wxwin.m4
# For other build systems the `wxwidgets_get_config` function can be used to get
# the absolute path to the `wx-config` binary
#
# exparams:
#   abi (format: x.y) (defaults to 3.0)
#     the wxGTK API/ABI to use
#
#   min_version (format: a.b.c) (empty by default)
#     minimal required version
#
#   wxwidgets_opts (format: "[foo][bar]") (empty by default)
#     option requirements for x11-libs/wxGTK
#
#   gtk_backends (format: array of supported gtk versions) (defaults to [ 3 ])
#     supported GTK+ backend versions
#
#   with_opt (format: true or false) (defaults to false)
#     whether an option needs to be enabled to use the wxWidgets toolkit
#
#   option_name (format: foo) (defaults to wxwidgets)
#     the name of the option that needs to be enabled to use the wxWidgets toolkit
#
# examples:
#
#   require wxwidgets [ min_versions="3.0.2" with_opt=true ]
#
#   generates:
#
#   MYOPTIONS="wxwidgets"
#
#   DEPENDENCIES="
#       build+run:
#           wxwidgets? ( x11-libs/wxGTK:3.0[>=3.0.2][providers:gtk3] )
#   "
#
#   ---
#
#   require wxwidgets [ wxwidgets_opts="[tiff]" gtk_backends=[ 2 3 ] ]
#
#   generates:
#
#   MYOPTIONS="( providers: gtk2 gtk3 ) [[ number-selected = exactly-one ]]"
#
#   DEPENDENCIES="
#       build+run:
#           x11-libs/wxGTK:3.0[providers:gtk2?][providers:gtk3?][tiff]
#   "

myexparam abi=3.0
myexparam min_version=
myexparam wxwidgets_opts=
myexparam gtk_backends=[ 3 ]
myexparam -b with_opt=false

exparam -b with_opt && myexparam option_name=wxwidgets

exparam -v WXWIDGETS_ABI abi
exparam -v WXWIDGETS_MIN_VERSION min_version
exparam -v WXWIDGETS_OPTS wxwidgets_opts
exparam -v WXWIDGETS_GTK_BACKENDS gtk_backends[@]

if exparam -b with_opt; then
    exparam -v OPTION_NAME option_name
    MYOPTIONS="${OPTION_NAME}"
fi

if [[ ${#WXWIDGETS_GTK_BACKENDS[@]} -gt 1 ]]; then
    MYOPTIONS+=" ( providers: "
    for GTK_VERSION in ${WXWIDGETS_GTK_BACKENDS[@]}; do
        MYOPTIONS+="
            gtk${GTK_VERSION} [[ description = [ Use wxWidgets built against GTK+ ${GTK_VERSION} ] ]]
        "
    done
    MYOPTIONS+=" ) [[ number-selected = exactly-one ]]"
fi

export_exlib_phases pkg_setup

DEPENDENCIES+="build+run: ( "
exparam -b with_opt && DEPENDENCIES+="${OPTION_NAME}? "
DEPENDENCIES+="( "
DEPENDENCIES+="x11-libs/wxGTK:${WXWIDGETS_ABI}"
[[ -n ${WXWIDGETS_MIN_VERSION} ]] && DEPENDENCIES+="[>=${WXWIDGETS_MIN_VERSION}]"
if [[ ${#WXWIDGETS_GTK_BACKENDS[@]} -gt 1 ]]; then
    for GTK_VERSION in ${WXWIDGETS_GTK_BACKENDS[@]}; do
        DEPENDENCIES+="[providers:gtk${GTK_VERSION}?]"
    done
else
    DEPENDENCIES+="[providers:gtk${WXWIDGETS_GTK_BACKENDS[@]}]"
fi
DEPENDENCIES+="${WXWIDGETS_OPTS} ) )"

_wxconfig_for_version() {
    local wxconfig wx_version=${1} gtk_version=${2}

    wxconfig=gtk${gtk_version}-unicode-
    ever at_least 2.9 ${wx_version} || wxconfig+=release-
    wxconfig+=${wx_version}

    echo -n "/usr/$(exhost --target)/lib/wx/config/${wxconfig}"
}

wxwidgets_get_config() {
    illegal_in_global_scope

    local backend
    if [[ ${#WXWIDGETS_GTK_BACKENDS[@]} -gt 1 ]]; then
        backend=$(option providers:gtk3 3 2)
    else
        backend=${WXWIDGETS_GTK_BACKENDS[@]}
    fi

    _wxconfig_for_version ${WXWIDGETS_ABI} ${backend}
}

wxwidgets_pkg_setup() {
    if { ! exparam -b with_opt || { exparam -b with_opt && option ${OPTION_NAME} ; } }; then
        local wxconfig
        wxconfig=$(wxwidgets_get_config)
        echo "Using wxWidgets: ${WXWIDGETS_ABI} (config: ${wxconfig})"
        export WX_CONFIG_NAME=${wxconfig}
    fi

    default
}

