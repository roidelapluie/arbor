# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'swig-1.3.35.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require sourceforge [ suffix=tar.gz ]
require ruby [ multibuild=false with_opt=true ]

SUMMARY="Simplified Wrapper and Interface Generator"
HOMEPAGE="http://www.${PN}.org"

REMOTE_IDS+=" freecode:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/Doc$(ever range 1-2)/SWIGDocumentation.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/Release/RELEASENOTES [[ lang = en ]]"

LICENCES="GPL-3 as-is"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    lua
    perl
    python
"

RESTRICT="test"

# TODO: python3 has an extra switch
DEPENDENCIES="
    build+run:
        dev-libs/pcre
        lua? ( dev-lang/lua:= )
        perl? ( dev-lang/perl:=[=5*] )
        python? ( dev-lang/python:=[=2*] )
"
#    test-expensive:
#        dev-libs/boost[>=1.20.0]
#"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( NEW FUTURE )

src_configure() {
    local myconf=()

    myconf+=(
        --with-pcre
        --without-{allegrocl,chicken,clisp,csharp,d,gcj,go,guile,java,javascript,mono,mzscheme}
        --without-{ocaml,octave,php,pike,python3,r,scilab,tcl}
        --without-doc
        # Breaks tests
        --disable-ccache
        $(option_enable lua)
        $(option_enable perl perl5)
        $(option_enable python)
        $(option_enable ruby ruby ruby$(ruby_get_abi))
    )

    econf "${myconf[@]}"
}

# TODO/FIXME: teach tests to use prefixed tools
#src_test_expensive() {
#    emake check
#}

src_install() {
    default

    if option doc; then
        dodir /usr/share/doc/${PNVR}
        insinto /usr/share/doc/${PNVR}
        doins -r Doc/{Devel,Manual}
    fi
}

