# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2015 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ check_target="check-${PN}" asserts=true ]

export_exlib_phases src_install

SUMMARY="OpenMP runtime for clang"

MYOPTIONS="
    hwloc [[ description = [ Use the hwloc library for affinity ] ]]
    ( libc: glibc musl ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        hwloc? ( dev-libs/hwloc )
"

case "$(exhost --target)" in
    x86_64*)
        OPENMP_ARCH="x86_64"
        ;;
    i*86-*)
        OPENMP_ARCH="i386"
        ;;
    arm*)
        OPENMP_ARCH="arm"
        ;;
    powerpc64*)
        OPENMP_ARCH="ppc64"
        ;;
    aarch64*)
        OPENMP_ARCH="aarch64"
        ;;
esac

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLIBOMP_ARCH:STRING=${OPENMP_ARCH}
    -DLIBOMP_ENABLE_SHARED:BOOL=TRUE
    -DOPENMP_ENABLE_OMPT_TOOLS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'asserts LIBOMP_ENABLE_ASSERTIONS'
    'hwloc LIBOMP_USE_HWLOC'
)

openmp_src_install() {
    cmake_src_install

    # We have sys-libs/libgomp, remove symlink
    rm ${IMAGE}/usr/$(exhost --target)/lib/libgomp.so
}

