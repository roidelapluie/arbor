# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ slotted=true ]
require alternatives providers

SUMMARY="The LLVM linker"

DEPENDENCIES+="
    build+run:
        !sys-devel/lld:0 [[
            description = [ Alternatives conflict ]
            resolution = uninstall-blocked-before
        ]]
    post:
        app-admin/eclectic-lld[targets:*(-)?]
"

# lld is a cross linker and can compile for any target without a
# new binary being compiled for the target. this is only used for the
# creation of the symlinks for targets; /usr/${CHOST}/bin/${target}-lld
CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS+="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

providers=( "llvm $(ever major)" )

export_exlib_phases src_configure src_install src_test

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLLVM_CONFIG_PATH:STRING=${LLVM_PREFIX}/bin/llvm-config

    # install LLVM to a slotted directory to prevent collisions with other llvm's
    -DCMAKE_INSTALL_PREFIX:STRING=${LLVM_PREFIX}
    -DCMAKE_INSTALL_MANDIR:STRING=${LLVM_PREFIX}/share/man
)

lld_src_configure() {
    providers_set
    cmake_src_configure
}

lld_target() {
    case "${1}" in
        aarch64-*)
            echo "aarch64elf"
            ;;
        armv7-*)
            echo "armelf"
            ;;
        i686-*)
            echo "elf_i386"
            ;;
        powerpc64-*)
            echo "elf64ppc"
            ;;
        x86_64-*)
            echo "elf_x86_64"
            ;;
    esac
}

lld_src_install() {
    local host=$(exhost --target)
    local target

    cmake_src_install

    # ban ld.lld, alternative for banned ld
    dobanned ld.lld

    local alternatives=( "${BANNEDDIR}"/ld.lld ld.lld-${SLOT} )

    edo pushd "${IMAGE}${LLVM_PREFIX}"/bin
    for bin in *; do
        alternatives+=( /usr/$(exhost --target)/bin/${bin} "${LLVM_PREFIX}"/bin/${bin} )
    done
    edo popd

    # provide host-prefixed ld.lld
    for target in ${CROSS_COMPILE_TARGETS}; do
        if option targets:${target}; then
            herebin ${target}-ld.lld << EOF
#!/usr/bin/env sh
exec "${LLVM_PREFIX}"/bin/ld.lld -m $(lld_target ${target}) "\${@}"
EOF
            alternatives+=( /usr/$(exhost --target)/bin/${target}-ld.lld "${LLVM_PREFIX}"/bin/${target}-ld.lld )
        fi
    done

    alternatives_for lld ${SLOT} ${SLOT} "${alternatives[@]}"

    edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
}

lld_src_test() {
    # There doesn't seem to be a test target
    :
}
