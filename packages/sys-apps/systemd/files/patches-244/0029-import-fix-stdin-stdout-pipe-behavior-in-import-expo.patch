From cfced59a4bd88e7d77ecf9ed40a22ec215356a83 Mon Sep 17 00:00:00 2001
From: Anita Zhang <the.anitazha@gmail.com>
Date: Tue, 17 Dec 2019 01:08:04 -0800
Subject: [PATCH 029/104] [import] fix stdin/stdout pipe behavior in
 import/export tar/raw

The code existed in machinectl to use stdin/stdout if the path for
import/export tar/raw was empty or dash (-) but a check to
`fd_verify_regular` in importd prevented it from working.

Update the check instead to explicitly check for regular file or
pipe/fifo.

Fixes #14346

(cherry picked from commit 1209ef94bd09bdc67a7b51f084910a5982f2f010)
---
 src/import/importd.c             | 18 ++++++++++++------
 test/TEST-25-IMPORT/testsuite.sh | 12 ++++++++++++
 2 files changed, 24 insertions(+), 6 deletions(-)

diff --git a/src/import/importd.c b/src/import/importd.c
index 93d89a3a1f..a75ab6bc07 100644
--- a/src/import/importd.c
+++ b/src/import/importd.c
@@ -694,6 +694,7 @@ static int method_import_tar_or_raw(sd_bus_message *msg, void *userdata, sd_bus_
         const char *local, *object;
         Manager *m = userdata;
         TransferType type;
+        struct stat st;
         uint32_t id;
 
         assert(msg);
@@ -717,9 +718,11 @@ static int method_import_tar_or_raw(sd_bus_message *msg, void *userdata, sd_bus_
         if (r < 0)
                 return r;
 
-        r = fd_verify_regular(fd);
-        if (r < 0)
-                return r;
+        if (fstat(fd, &st) < 0)
+                return -errno;
+
+        if (!S_ISREG(st.st_mode) && !S_ISFIFO(st.st_mode))
+                return -EINVAL;
 
         if (!machine_name_is_valid(local))
                 return sd_bus_error_setf(error, SD_BUS_ERROR_INVALID_ARGS, "Local name %s is invalid", local);
@@ -829,6 +832,7 @@ static int method_export_tar_or_raw(sd_bus_message *msg, void *userdata, sd_bus_
         const char *local, *object, *format;
         Manager *m = userdata;
         TransferType type;
+        struct stat st;
         uint32_t id;
 
         assert(msg);
@@ -855,9 +859,11 @@ static int method_export_tar_or_raw(sd_bus_message *msg, void *userdata, sd_bus_
         if (!machine_name_is_valid(local))
                 return sd_bus_error_setf(error, SD_BUS_ERROR_INVALID_ARGS, "Local name %s is invalid", local);
 
-        r = fd_verify_regular(fd);
-        if (r < 0)
-                return r;
+        if (fstat(fd, &st) < 0)
+                return -errno;
+
+        if (!S_ISREG(st.st_mode) && !S_ISFIFO(st.st_mode))
+                return -EINVAL;
 
         type = streq_ptr(sd_bus_message_get_member(msg), "ExportTar") ? TRANSFER_EXPORT_TAR : TRANSFER_EXPORT_RAW;
 
diff --git a/test/TEST-25-IMPORT/testsuite.sh b/test/TEST-25-IMPORT/testsuite.sh
index 380ba3d82d..d4efd71e06 100755
--- a/test/TEST-25-IMPORT/testsuite.sh
+++ b/test/TEST-25-IMPORT/testsuite.sh
@@ -119,6 +119,18 @@ machinectl remove scratch4
 ! test -f /var/lib/machines/scratch4
 ! machinectl image-status scratch4
 
+# Test import-tar hypen/stdin pipe behavior
+cat /var/tmp/scratch.tar.gz | machinectl import-tar - scratch5
+test -d /var/lib/machines/scratch5
+machinectl image-status scratch5
+diff -r /var/tmp/scratch/ /var/lib/machines/scratch5
+
+# Test export-tar hypen/stdout pipe behavior
+mkdir -p /var/tmp/extract
+machinectl export-tar scratch5 - | tar xvf - -C /var/tmp/extract/
+diff -r /var/tmp/scratch/ /var/tmp/extract/
+rm -rf /var/tmp/extract
+
 rm -rf /var/tmp/scratch
 
 echo OK > /testok
-- 
2.25.1

